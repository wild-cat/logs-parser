import {getClientMessageType, getSessionID, getTimestamp} from '../../../lib/utils/test_parse_utils';

describe ('ParseUtils', function () {
    let testData: string;

    describe('getTimestamp', function () {
        it('should return timestamp from a string', function () {
            testData = '2016-03-31T22:31:29.842Z - debug -';
            expect(getTimestamp(testData)).toBe('2016-03-31T22:31:29.842Z');
        });
        it('should return null on empty string', function () {
            testData = '';
            expect(getTimestamp(testData)).toBeNull();
        });
        it('should return timestamp from the middle of a string', function () {
            testData = 'server1/folder/forlder: 2016-03-31T22:31:29.842Z - debug -';
            expect(getTimestamp(testData)).toBe('2016-03-31T22:31:29.842Z');
        });
        it('should return timestamp from the end of a string', function () {
            testData = 'server1/folder/forlder: 2016-03-31T22:31:29.842Z';
            expect(getTimestamp(testData)).toBe('2016-03-31T22:31:29.842Z');
        });
        it('should return first occurence of the timestamp if many', function () {
            testData = 'server1/folder/forlder: 2016-03-31T22:31:29.842Z - debug - 1995-06-30T01:01:01.000Z';
            expect(getTimestamp(testData)).toBe('2016-03-31T22:31:29.842Z');
        });
    });

    describe('getClientMessageType', function () {
        it('should return message type from a string', function () {
            testData = '- debug - send to client  type: dfx_technicians data: ';
            expect(getClientMessageType(testData)).toBe('dfx_technicians');
        });
        it('should return null on empty string', function () {
            testData = '';
            expect(getClientMessageType(testData)).toBeNull();
        });
        it('should return message type if it contains - and digits', function () {
            testData = 'send to client  type: type-159 data: ';
            expect(getClientMessageType(testData)).toBe('type-159');
        });
    });

    describe('getSessionID', function () {
        it('should return session id from a string', function () {
            testData = ' - sessionId: 500f9d20-f790-11e5-8bc4-8353023ccf57; ';
            expect(getSessionID(testData)).toBe('500f9d20-f790-11e5-8bc4-8353023ccf57');
        });
        it('should return null on empty string', function () {
            testData = '';
            expect(getSessionID(testData)).toBeNull();
        });
        it('should return null on string with incorrect session id', function () {
            testData = 'sessionId: 500f9d20-f790-11e5';
            expect(getSessionID(testData)).toBeNull();
        });
    });
});