import {TestRawParser} from '../../../lib/raw_parsers/test_raw_parser';
import * as sinon from 'sinon';

describe('RawParser', function() {
    let rawParser: TestRawParser;
    let testData: string;
    let getTimestamp: Sinon.SinonStub;
    let getClientMessageType: Sinon.SinonStub;
    let getSessionID: Sinon.SinonStub;

    beforeEach(function() {
        getTimestamp = sinon.stub();
        getClientMessageType = sinon.stub();
        getSessionID = sinon.stub();
        rawParser = new TestRawParser(getClientMessageType, getTimestamp, getSessionID);
        testData = 'abc';
    });

    /* positive cases */
    describe('parse', function() {
        it('should return proper object in common case', function() {
            getClientMessageType.returns('dfx_technicians');
            getSessionID.returns('500f9d20-f790-11e5-8bc4-8353023ccf57');
            getTimestamp.returns('2016-03-31T22:31:29.842Z');
            let res = rawParser.parse(testData);
            expect(res.date).toBe('2016-03-31T22:31:29.842Z');
            expect(res.sessionId).toBe('500f9d20-f790-11e5-8bc4-8353023ccf57');
            expect(res.type).toBe('dfx_technicians');
        });
        it('should return null if parameter is empty string', function() {
            expect(rawParser.parse('')).toBeNull();
        });
        it('should return null if getClientMessageType returns undefined', function() {
            getClientMessageType.returns(undefined);
            expect(rawParser.parse(testData)).toBeNull();
        });
        it('should return null if getClientMessageType returns null', function() {
            getClientMessageType.returns(null);
            expect(rawParser.parse(testData)).toBeNull();
        });

        /* error cases */
        it('should throws an error if getTimestamp throws an error', function() {
            getClientMessageType.returns('abc');
            getTimestamp.throws(new Error('getTimestamp error'));
            expect(rawParser.parse).toThrow();
        });
        it('should throws an error if getClientMessageType throws an error', function() {
            getClientMessageType.throws(new Error('getClientMessageType error'));
            expect(rawParser.parse).toThrow();
        });
        it('should throws an error if getSessionID throws an error', function() {
            getClientMessageType.returns('abc');
            getSessionID.throws(new Error('getSessionID error'));
            expect(rawParser.parse).toThrow();
        });
        it('should throws an error if getTimestamp returns not string', function() {
            getClientMessageType.returns('abc');
            getTimestamp.returns({foo: 'bar'});
            expect(rawParser.parse).toThrow();
        });
        it('should throws an error if getClientMessageType returns not string', function() {
            getClientMessageType.returns({foo: 'bar'});
            expect(rawParser.parse).toThrow();
        });
        it('should throws an error if getSessionID returns not string', function() {
            getClientMessageType.returns('abc');
            getSessionID.returns({foo: 'bar'});
            expect(rawParser.parse).toThrow();
        });
    });

});
