import {SyncFileWriter} from '../../lib/sync_file_writer';
import * as sinon from 'sinon';
import {FsStub} from '../stubs/fs_stub';
import {PromiseHandler} from '../../lib/interfaces';

describe('SyncFileWriter', function() {
    let outFile = './out.txt';
    let testData: string;
    let syncFileWriter: SyncFileWriter<string>;
    let fs: FsStub;
    let mockResolveFn: PromiseHandler;
    let mockRejectFn: PromiseHandler;

    beforeEach(function() {
        fs = new FsStub(sinon);
        syncFileWriter = new SyncFileWriter(fs);
    });

    describe('saveData', function() {

        /* positive cases */
        it('should call writeFileSync from standard library with correct parameters', function(done) {
            testData = 'something';

            mockResolveFn = (result) => {
                expect(result).toEqual(true);
                expect(fs.stubs.writeFileSync.calledWith(outFile, testData)).toBe(true);
            };

            mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };

            syncFileWriter.saveData(outFile, testData).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });

        /* error cases */
        it('should call rejected handler with the same error that fs module throws', function(done) {
            let exception = new Error('Test error');

            fs.stubs.writeFileSync.throws(exception);

            mockResolveFn = (result) => {
                expect(result).toBeUndefined();
            };

            mockRejectFn = (err) => {
                expect(err).toBe(exception);
            };

            syncFileWriter.saveData(outFile, testData).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
    });
});