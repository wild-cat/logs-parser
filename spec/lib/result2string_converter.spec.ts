import {ResultToStringConverter} from '../../lib/result2string_converter';
import * as sinon from 'sinon';
import {JsonStub} from '../stubs/json_stub';
import {PromiseHandler} from '../../lib/interfaces';

describe('ResultToStringConverter', function() {
    let converter: ResultToStringConverter<any>;
    let testData: Array<any>;
    let testResult: string;
    let json: JsonStub;
    let mockResolveFn: PromiseHandler;
    let mockRejectFn: PromiseHandler;
    let exception: Error;

    beforeEach(function() {
        json = new JsonStub(sinon);
        converter = new ResultToStringConverter(json);
    });

    describe('convert', function() {

        /* positive cases */
        it('should return proper result', function(done) {
            testData = [{foo: 'bar'}];
            testResult = '[{foo: "bar"}]';

            json.stubs.stringify.returns(testResult);
            mockResolveFn = (data) => {
                expect(json.stubs.stringify.calledOnce).toBe(true);
                expect(json.stubs.stringify.calledWith(testData)).toBe(true);
                expect(data).toBe(testResult);
            };
            mockRejectFn = (err) => {
                expect('Exception not').toBe('happened');
            };
            converter.convert(testData).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should return [] if parameter is empty array', function(done) {
            testData = [];
            testResult = '[]';

            json.stubs.stringify.returns(testResult);
            mockResolveFn = (data) => {
                expect(json.stubs.stringify.calledOnce).toBe(true);
                expect(json.stubs.stringify.calledWith(testData)).toBe(true);
                expect(data).toBe(testResult);
            };
            mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            converter.convert(testData).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });


        /* error cases */
        it('should properly catch an error', function(done) {
            testData = [{foo: 'bar'}];
            exception = new Error('JSON stringify exception');
            json.stubs.stringify.throws(exception);

            mockResolveFn = (data) => {
                expect(data).toBeUndefined();
            };
            mockRejectFn = (err) => {
                expect(err).toBe(exception);
            };

            converter.convert(testData).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
    });
});