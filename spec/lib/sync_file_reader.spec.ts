import * as sinon from 'sinon'
import {FsStub} from '../stubs/fs_stub';
import {SyncFileReader} from '../../lib/sync_file_reader';
import {RawParserStub} from '../stubs/raw_parser_stub';
import {PromiseHandler} from '../../lib/interfaces';

describe('SyncFileReader', function () {

    let inputFile = './infile.txt';
    let fileReader: SyncFileReader<any>;
    let rawParser: RawParserStub<any>;
    let testResult: any;
    let fs: FsStub;
    let testData: Array<string>;
    let mockResolveFn: PromiseHandler;
    let mockRejectFn: PromiseHandler;

    beforeEach(function () {
        testResult = {result: "done"};
        fs = new FsStub(sinon);
        rawParser = new RawParserStub(sinon);
        fileReader = new SyncFileReader(rawParser, fs);
    });

    describe('readData', function () {

        /* positive cases */
        it('should return as many items as read from file', function(done) {
            testData = ['abc', 'cde', 'efg'];

            fs.stubs.readFileSync.returns(testData.join('\n'));
            rawParser.stubs.parse.onFirstCall().returns('abc');
            rawParser.stubs.parse.onSecondCall().returns('cde');
            rawParser.stubs.parse.onThirdCall().returns('efg');

            mockResolveFn = (data) => {
                expect(data).toEqual(['abc', 'cde', 'efg']);
            };
            mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should return [] if rawParser returns []', function(done) {
            fs.stubs.readFileSync.returns('');
            rawParser.stubs.parse.returns([]);

            let mockResolveFn = (data) => {
                expect(data).toEqual([[]]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };

            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should return proper result if input file is empty', function(done) {
            fs.stubs.readFileSync.returns('');
            rawParser.stubs.parse.returns('');

            let mockResolveFn = (data) => {
                expect(data).toEqual(['']);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };

            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should not ignore empty lines in input file', function(done) {
            testData = ['one', '', 'three'];
            fs.stubs.readFileSync.returns(testData.join('\n'));
            rawParser.stubs.parse.onFirstCall().returns('one');
            rawParser.stubs.parse.onSecondCall().returns('');
            rawParser.stubs.parse.onThirdCall().returns('three');

            let mockResolveFn = (data) => {
                expect(data).toEqual(['one', '', 'three']);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };

            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should work well with only one line in input file', function(done) {
            testData = ['one'];
            fs.stubs.readFileSync.returns(testData.join('\n'));
            rawParser.stubs.parse.returns('one');

            let mockResolveFn = (data) => {
                expect(data).toEqual(['one']);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };

            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should be able to add numbers to results', function(done) {
            fs.stubs.readFileSync.returns('abc');
            rawParser.parse = sinon.stub().returns(5);

            let mockResolveFn = (data) => {
                expect(data).toEqual([5]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should be able to add zero to results', function(done) {
            fs.stubs.readFileSync.returns('abc');
            rawParser.parse = sinon.stub().returns(0);

            let mockResolveFn = (data) => {
                expect(data).toEqual([0]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should be able to add objects to results', function(done) {
            fs.stubs.readFileSync.returns('abc');
            rawParser.parse = sinon.stub().returns({one:'two'});

            let mockResolveFn = (data) => {
                expect(data).toEqual([{one:'two'}]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should not add undefined to results', function(done) {
            fs.stubs.readFileSync.returns('abc');
            rawParser.parse = sinon.stub().returns(undefined);

            let mockResolveFn = (data) => {
                expect(data).toEqual([]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should not add null to results ', function(done) {
            fs.stubs.readFileSync.returns('abc');
            rawParser.parse = sinon.stub().returns(null);

            let mockResolveFn = (data) => {
                expect(data).toEqual([]);
            };
            let mockRejectFn = () => {
                expect('Exception not').toBe('happened');
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });

        /* error cases */
        it('should return the same error as File System throws', function(done) {
            let exception = new Error("File system error");
            fs.stubs.readFileSync.throws(exception);

            let mockResolveFn = (data) => {
                expect(data).toBeUndefined();
            };
            let mockRejectFn = (err) => {
                expect(err).toBe(exception);
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
        it('should return the same error as rawParser throws', function(done) {
            testData = ['abc', 'cde', 'efg'];
            fs.stubs.readFileSync.returns(testData.join('\n'));

            let exception = new Error('rawParser error');
            rawParser.parse = sinon.stub().throws(exception);

            let mockResolveFn = (data) => {
                expect(data).toBeUndefined();
            };
            let mockRejectFn = (err) => {
                expect(err).toBe(exception);
            };
            fileReader.readData(inputFile).then(mockResolveFn).catch(mockRejectFn).finally(done);
        });
    });
});