import {TestLogParser} from '../../lib/test_log_parser';
import {DataReaderStub} from '../stubs/data_reader_stub';
import {DataWriterStub} from '../stubs/data_writer_stub';
import {ResultConverterStub} from '../stubs/result_converter_stub';
import {PromiseHandler} from '../../lib/interfaces';

import * as bluebird from 'bluebird';
import * as sinonAsPromised from 'sinon-as-promised';
let sinon = sinonAsPromised(bluebird);

describe('LogParser', function() {
    let inputFile = './test.txt';
    let outFile = './out.txt';

    let dataReader: DataReaderStub<any>;
    let dataWriter: DataWriterStub<any>;
    let resultConverter: ResultConverterStub<any>;
    let logParser: TestLogParser<any>;

    let converterResult: any;
    let writerResult: boolean;
    let readerResult: any;

    let resolvePromiseHandler: PromiseHandler;
    let rejectPromiseHandler: PromiseHandler;

    let exception: Error;

    beforeEach(function() {
        readerResult = [{abc: '123'}];
        converterResult = '[{"abc": "123"}]';
        writerResult = true;

        dataReader = new DataReaderStub(sinon);
        dataWriter = new DataWriterStub(sinon);
        resultConverter = new ResultConverterStub(sinon);

        logParser = new TestLogParser(dataReader, dataWriter, resultConverter);
    });

    describe('process', function() {

        /* positive cases */
        it('should read, parse and save results in positive case', function(done) {
            resolvePromiseHandler = (result) => {
                expect(result).toBe(true);
                expect(dataReader.stubs.readData.called).toBe(true);
                expect(dataReader.stubs.readData.calledWith(inputFile)).toBe(true);
                expect(resultConverter.stubs.convert.called).toBe(true);
                expect(resultConverter.stubs.convert.calledWith(readerResult));
                expect(dataWriter.stubs.saveData.called).toBe(true);
                expect(dataWriter.stubs.saveData.calledWith(outFile, converterResult)).toBe(true);
            };

            rejectPromiseHandler = () => {
                expect('Exception not').toBe('happened');
            };

            dataReader.stubs.readData.resolves(readerResult);
            resultConverter.stubs.convert.resolves(converterResult);
            dataWriter.stubs.saveData.resolves(writerResult);

            logParser.process(inputFile, outFile).then(resolvePromiseHandler).catch(rejectPromiseHandler).finally(done);

        });

        /* error cases */
        it('should return the same error that DataReader returns', function(done) {
            exception = new Error('Test Exception');

            dataReader.stubs.readData.rejects(exception);

            resolvePromiseHandler = () => {
                expect('Resolve not').toBe('happened');
            };

            rejectPromiseHandler = (err) => {
                expect(err).toBe(exception);
                expect(dataReader.stubs.readData.calledOnce).toBe(true);
                expect(resultConverter.stubs.convert.called).toBe(false);
                expect(dataWriter.stubs.saveData.called).toBe(false);
            };

            logParser.process(inputFile, outFile).then(resolvePromiseHandler).catch(rejectPromiseHandler).finally(done);
        });
        it('should return the same error that DataWriter returns', function(done) {
            exception = new Error('DataWriter exception');

            dataReader.stubs.readData.resolves(readerResult);
            resultConverter.stubs.convert.resolves(converterResult);
            dataWriter.stubs.saveData.rejects(exception);

            resolvePromiseHandler = () => {
                expect('Resolve not').toBe('happened');
            };

            rejectPromiseHandler = (err) => {
                expect(err).toBe(exception);
                expect(dataReader.stubs.readData.calledOnce).toBe(true);
                expect(dataReader.stubs.readData.calledWith(inputFile)).toBe(true);
                expect(resultConverter.stubs.convert.calledOnce).toBe(true);
                expect(dataWriter.stubs.saveData.calledOnce).toBe(true);
            };

            logParser.process(inputFile, outFile).then(resolvePromiseHandler).catch(rejectPromiseHandler).finally(done);
        });
        it('should return the same error that ResultToStringConverter returns', function(done) {
            exception = new Error('ResultToStringConverter exception');

            dataReader.stubs.readData.resolves(readerResult);
            resultConverter.stubs.convert.rejects(exception);

            resolvePromiseHandler = () => {
                expect('Resolve not').toBe('happened');
            };

            rejectPromiseHandler = (err) => {
                expect(err).toBe(exception);
                expect(dataReader.stubs.readData.calledOnce).toBe(true);
                expect(dataReader.stubs.readData.calledWith(inputFile)).toBe(true);
                expect(resultConverter.stubs.convert.calledOnce).toBe(true);
                expect(dataWriter.stubs.saveData.called).toBe(false);
            };

            logParser.process(inputFile, outFile).then(resolvePromiseHandler).catch(rejectPromiseHandler).finally(done);
        });
    });

});