describe('log-parser', function () {
    require('./lib/utils/test_parse_utils.spec');
    require('./lib/raw_parsers/test_raw_parser.spec');
    require('./lib/sync_file_writer.spec');
    require('./lib/sync_file_reader.spec');
    require('./lib/result2string_converter.spec');
    require('./lib/test_log_parser.spec');
});