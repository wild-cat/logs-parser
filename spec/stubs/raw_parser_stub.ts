import {RawParser} from '../../lib/interfaces'

export class RawParserStub<T> implements RawParser<T>{
    stubs: {
        parse: Sinon.SinonStub
    };

    constructor(sinon: Sinon.SinonStatic) {
        this.stubs = {
            parse: sinon.stub()
        };
    }

    parse(raw: string): T {
        return this.stubs.parse(raw);
    }
}
