import {DataReader} from '../../lib/interfaces'
import SinonStubPromised = SinonAsPromise.SinonStubPromised;

export class DataReaderStub<T> implements DataReader<T> {
    stubs: {
        readData: SinonStubPromised<any>
    };

    constructor(sinon) {
        this.stubs = {
            readData: sinon.stub()
        };
    }

    readData(filename: string): Promise<any> {
        return this.stubs.readData(filename);
    }
}