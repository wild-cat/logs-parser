import {Json} from 'JSON'

export class JsonStub implements Json {
    stubs: {
        stringify: Sinon.SinonStub;
        parse: Sinon.SinonStub;
    };

    constructor(sinon: Sinon.SinonStatic) {
        this.stubs = {
            stringify: sinon.stub(),
            parse: sinon.stub()
        }
    }

    stringify(obj: any, replacer?: Function, space?: string): string
    stringify(obj: any, replacer?: Function, space?: number): string
    stringify(obj: any, replacer?: Array<string>, space?: string): string
    stringify(obj: any, replacer?: Array<string>, space?: number): string
    stringify(...args: Array<any>): string {
        return this.stubs.stringify(...args);
    }

    parse(str: string, reviver?: (key: string, value: string) => any): any {
        return this.stubs.parse(str, reviver)
    }
}
