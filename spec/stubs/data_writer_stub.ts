import {DataWriter} from '../../lib/interfaces'
import SinonStubPromised = SinonAsPromise.SinonStubPromised;

export class DataWriterStub<T> implements DataWriter<T> {
    stubs: {
        saveData: SinonStubPromised<any>
    };

    constructor(sinonAsPromised) {
        this.stubs = {
            saveData: sinonAsPromised.stub()
        };
    }

    saveData(filename: string, data: T): Promise<T> {
        return this.stubs.saveData(filename, data);
    }
}