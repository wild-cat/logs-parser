import {ResultConverter} from '../../lib/interfaces'
import SinonStubPromised = SinonAsPromise.SinonStubPromised;

export class ResultConverterStub<T> implements ResultConverter<T> {
    stubs: {
        convert: SinonStubPromised<any>
    };

    constructor(sinonAsPromised) {
        this.stubs = {
            convert: sinonAsPromised.stub()
        };
    }

    convert(logDetails: Array<T>): Promise<T> {
        return this.stubs.convert(logDetails);
    }
}