/**
 * Created by yusup.abdullaev on 01.02.2016.
 */

import { Stats, FSWatcher, FileReadSync, FileReadAsync, FileWriteSync, FileWriteAsync, WatchFile } from 'fs';

export class FsStub implements FileReadSync, FileReadAsync, FileWriteSync, FileWriteAsync, WatchFile {
    stubs: {
        readFileSync: Sinon.SinonStub;
        readFile: Sinon.SinonStub;
        writeFileSync: Sinon.SinonStub;
        writeFile: Sinon.SinonStub;
        watchFile: Sinon.SinonStub;
        unwatchFile: Sinon.SinonStub;
        watch: Sinon.SinonStub;
    };

    constructor(sinon: Sinon.SinonStatic) {
        this.stubs = {
            readFileSync: sinon.stub(),
            readFile: sinon.stub(),
            writeFileSync: sinon.stub(),
            writeFile: sinon.stub(),
            watchFile: sinon.stub(),
            unwatchFile: sinon.stub(),
            watch: sinon.stub(),
        };
    }

    readFileSync(filename: string): Buffer;
    readFileSync(filename: string, encoding: string): string;
    readFileSync(...args: Array<any>): any {
        return this.stubs.readFileSync(...args);
    }

    readFile(filename: string, encoding: string, callback: (err: ErrnoException, data: string) => void): void;
    readFile(filename: string, callback: (err: ErrnoException, data: Buffer) => void): void;
    readFile(...args): void {
        this.stubs.readFile(...args);
    }

    writeFileSync(filename: string, data: any, encoding?: string): void {
        return this.stubs.writeFileSync(filename, data, encoding);
    }

    writeFile(filename: string, data: any, callback?: (err)=>void): void;
    writeFile(filename: string, data: any, encoding?: string, callback?: (err)=>void): void;
    writeFile(...args: Array<any>): void {
        this.stubs.writeFile(...args);
    }

    watchFile(filename: string, listener: { curr: Stats, prev: Stats }): void;
    watchFile(filename: string, options: { persistent?: boolean, interval?: number },
              listener: { curr: Stats, prev: Stats }): void;
    watchFile(...args): void {
        this.stubs.watchFile(...args);
    }

    unwatchFile(filename: string, listener?: Stats): void {
        this.stubs.unwatchFile(filename, listener);
    }

    watch(filename: string, options?: { persistent?: boolean },
          listener?: (event: string, filename: string) => any): FSWatcher {
        return this.stubs.watch(filename, options, listener);
    }
}