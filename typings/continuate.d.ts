/**
 * Created by yusup.abdullaev on 22.01.2016.
 */

declare module Continuate {
    function cps<R>(f: () => R): (callback: (err: Error, result: R) => void) => void;
    function cps<T1, R>(f: (arg1: T1) => R): (arg1: T1, callback: (err: Error, result: R) => void) => void;
    function cps<T1, T2, R>(f: (arg1: T1, arg2: T2) => R): (arg1: T1, arg2: T2, callback: (err: Error, result: R) => void) => void;
    function cps<T1, T2, T3, R>(f: (arg1: T1, arg2: T2, arg3: T3) => R): (arg1: T1, arg2: T2, arg3: T3, callback: (err: Error, result: R) => void) => void;
    function cps<T1, T2, T3, T4, R>(f: (arg1: T1, arg2: T2, arg3: T3, arg4: T4) => R): (arg1: T1, arg2: T2, arg3: T3, arg4: T4, callback: (err: Error, result: R) => void) => void;
    function cps<T1, T2, T3, T4, T5, R>(f: (arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5) => R): (arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5, callback: (err: Error, result: R) => void) => void;
    function cps<T1, T2, T3, T4, T5, T6, R>(f: (arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5, arg6: T6) => R): (arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5, arg6: T6, callback: (err: Error, result: R) => void) => void;
}

declare module 'continuate' {
    var cps: typeof Continuate.cps;
    export = cps;
}