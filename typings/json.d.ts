declare module 'JSON' {
    export interface HasStringify {
        stringify(obj:any, replacer?:Function, space?:string): string
        stringify(obj:any, replacer?:Function, space?:number): string
        stringify(obj:any, replacer?:Array<string>, space?:string): string
        stringify(obj:any, replacer?:Array<string>, space?:number): string
    }

    export interface HasParse {
        parse(str:string, reviver?:(key:string, value:string) => any): any
    }

    export interface Json extends HasStringify, HasParse {

    }
}