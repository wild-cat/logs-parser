/**
 * Author: kirill.vladykin
 * Date: 01.06.2016
 */

declare module SinonAsPromise {

    interface PromiseThenFunction {
        (result: any): any
    }

    interface PromiseCatchFunction {
        (err: Error): any
    }

    interface Thenable<T> {
        then(func: PromiseThenFunction): Promise<T>
        catch(func: PromiseCatchFunction): Promise<T>
    }

    interface SinonStubPromised<T> extends Sinon.SinonStub {
        rejects(err: Error|string): Thenable<T>
        resolves(obj: any): Thenable<T>
    }

}

declare module "sinon-as-promised" {
    var SinonStubPromised: SinonAsPromise.SinonStubPromised<any>;
    export = SinonStubPromised;
}