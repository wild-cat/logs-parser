const TIMESTAMP_REG_EXP = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z/;
const CLIENT_TYPE_REG_EXP = /send to client  type: ([\w0-9-]+)/;
const SESSION_ID_REG_EXP = /sessionId: (\w{8}-\w{4}-\w{4}-\w{4}-\w{12});/;

export function getTimestamp(str: string): string {
    let matchArr = str.match(TIMESTAMP_REG_EXP);
    return (matchArr) ? csp(matchArr[0]) : null;
}

export function getClientMessageType(str: string): string {
    let matchArr = str.match(CLIENT_TYPE_REG_EXP);
    return (matchArr) ? csp(matchArr[1]) : null;
}

export function getSessionID(str: string): string {
    let matchArr = str.match(SESSION_ID_REG_EXP);
    return (matchArr) ? csp(matchArr[1]) : null;
}

function string2Bin(str: string): Array<number> {
    let result = [];
    for (let i = 0; i < str.length; i++) {
        result.push(str.charCodeAt(i));
    }
    return result;
}

function bin2String(array: Array<number>): string {
    return String.fromCharCode.apply(String, array);
}

function csp(str: string): string {
    return bin2String(string2Bin(str));
}