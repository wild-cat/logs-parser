import {ENodeCallback} from './types';
import {LogParser, DataReader, DataWriter, ResultConverter} from "./interfaces";

export class TestLogParser<T> implements LogParser {
    constructor(private dataReader: DataReader<T>,
                private dataWriter: DataWriter<T>,
                private resultConverter: ResultConverter<T>) {

    }

    process(inputFileFullName: string, outputFileFullName: string): Promise<any> {
        return this.dataReader.readData(inputFileFullName).then((rawData) => {
            return this.resultConverter.convert(rawData);
        }).then((convertedData) => {
            return this.dataWriter.saveData(outputFileFullName, convertedData);
        });

    }

}
