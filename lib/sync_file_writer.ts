import {DataWriter} from './interfaces';
import {ENodeCallback} from './types';
import {FileWriteSync} from 'fs';
import * as Promise from 'bluebird';

export class SyncFileWriter<T> implements DataWriter<T> {
    constructor(private fs: FileWriteSync) {

    }

    saveData(filename: string, data: T): Promise<any> {
        return new Promise((resolve) => {
            this.fs.writeFileSync(filename, data, 'utf-8');
            resolve(true);
        });
    }

}