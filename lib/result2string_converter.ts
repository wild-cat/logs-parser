import {ResultConverter} from './interfaces';
import {HasStringify} from "JSON";
import * as Promise from 'bluebird';

export class ResultToStringConverter<T> implements ResultConverter<T> {
    constructor(private json: HasStringify ) {

    }

    convert(logDetails: Array<T>): Promise<any> {
        return new Promise((resolve) => {
            resolve(this.json.stringify(logDetails));
        });
    }

}

