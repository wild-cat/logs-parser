/**
 * Created by artem.kolosovich on 15.01.2016.
 */

export interface StackError extends Error {
    stack: string;
}

export type NodeCallback<E, T> = (err?: E, result?: T) => void;
export type ENodeCallback<T> = NodeCallback<Error, T>;
export type StringMap<T> = { [K: string]: T };
export type NumberMap<T> = { [K: number]: T };
export type HashMap<T> = StringMap<T> | NumberMap<T>;