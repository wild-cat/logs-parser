import {ENodeCallback} from './types';

export interface DataReader<T> {
    readData(fileName: string): Promise<any>
}

export interface DataWriter<T> {
    saveData(filename: string, data: T): Promise<any>
}

export interface RawParser<T> {
    parse(raw: string): T
}

export interface ResultConverter<T> {
    convert(logDetails: Array<T>): Promise<any>;
}

export interface LogParser {
    process(inputFileFullName: string, outputFileFullName: string): Promise<any>
}

export interface PromiseHandler {
    (arg?: any): void
}