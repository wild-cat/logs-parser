import {DataReader, RawParser} from './interfaces';
import {ENodeCallback} from './types';
import {FileReadSync} from 'fs';
import * as Promise from 'bluebird';

export class SyncFileReader<T> implements DataReader<T> {
    constructor(private rawParser: RawParser<T>, private fs: FileReadSync) {

    }

    readData(fileName: string): Promise<any> {
        return new Promise((resolve) => {
            let data = this.fs.readFileSync(fileName, 'utf-8');
            let linesArr = data.split('\n');
            let res = [];
            for (let i = 0; i < linesArr.length; i++) {
                let objectParsed = this.rawParser.parse(linesArr[i]);
                if (objectParsed != null)
                    res.push(objectParsed);
            }
            resolve(res);
        });

    }

}
