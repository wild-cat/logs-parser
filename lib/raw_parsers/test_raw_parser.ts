import {RawParser} from '../interfaces'

export interface TestMessage {
    date: string,
    sessionId: string,
    type: string
}

type FnStr2Str = (arg: string) => string;

export class TestRawParser implements RawParser<TestMessage> {
    constructor(private getClientMessageType: FnStr2Str, private getTimestamp: FnStr2Str, private getSessionID: FnStr2Str) {

    }
    parse(raw: string): TestMessage {
        let messageType = this.getClientMessageType(raw);
        if (messageType == null) {
            return null;
        }
        return {
            date: this.getTimestamp(raw),
            sessionId: this.getSessionID(raw),
            type: messageType
        };
    }
}
