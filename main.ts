import * as fs from 'fs';
import {SyncFileReader, TestRawParser, ResultToStringConverter, SyncFileWriter, TestLogParser,
        getTimestamp, getClientMessageType, getSessionID} from './index';

let rawParser = new TestRawParser(getClientMessageType, getTimestamp, getSessionID);
let syncFileReader = new SyncFileReader(rawParser, fs);
let syncFileWriter = new SyncFileWriter(fs);
let resultToStringConverter = new ResultToStringConverter(JSON);
let logParser = new TestLogParser(syncFileReader, syncFileWriter, resultToStringConverter);

logParser.process("./logs/test_log.txt", "./output/log_parsed.txt").then(() => {
   console.log("Log parser tool: processed file successfully");
}).catch((err) => {
    console.error("Log parser tool: ", err.message, err.trace);
});
