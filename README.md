# Logs-parser tool
## Table of contents

[TOC]

## Quick summary 

Logs-parser tool is a NodeJS/TypeScript tool for parsing logs stored in text files and write information to other files. It is written with good modular structure in correspondence with SRP (Single Responsibility Principle). 

With little modification the tool can be consumed to transform information in any format to any another format, from any source to any another source.

version 1.0.0

## Callbacktypes

Several interfaces contain callback of type ENodeCallback<T> as a parameter of a method. There are two types: NodeCallback that optionally returns both *err* and *result* and ENodeCallback that always return *err* field and may return *result* 
**In this project** *err* field equals to javascript Error object or null. *Result* field returns array of some T type or true/false.
These types definitions are listed below: 
```
#!typescript

type NodeCallback<E, T> = (err?: E, result?: T) => void;
type ENodeCallback<T> = NodeCallback<Error, T>

```

## Interfaces

While particular classes provide concrete functionality, interfaces construct the core of the tool and make this tool reusable.

### DataReader 
It is an interface for a class that can read information from some source and return it as array of some type. 
```
#!typescript

interface DataReader<T> {
    readData(fileName: string, callback: ENodeCallback<T>): void
}

```

Implementations: *SyncDataReader* 

### RawParser 
It is an interface for a class that takes a string, parses it and returns data in some format.


```
#!typescript

interface RawParser<T> {
    parse(raw: string): T
}

```

Implementations: *TestRawParser*

### ResultConverter 
It's implementation is responsible for convertion an array of results into final representation.


```
#!typescript

interface ResultConverter<T> {
    convert(logDetails: Array<T>): string
}

```

Implementations: *ResultToStringConverter*

### DataWriter 
It's implementation in responsible for writing end data to the output source.


```
#!typescript

interface DataWriter<T>{
    saveData(filename: string, data: T, callback: ENodeCallback<boolean>): void
}

```

Implementations: *SyncFileWriter*

### LogParser 
It is an interface that declares a top-level wrap that must have a single top-level method *process* for doing tool's job.


```
#!typescript

interface LogParser<T>{
    process(inputFileFullName: string, outputFileFullName: string, callback: ENodeCallback<boolean>): void
}

```

Implementations: *TestLogParser*

## Example of usage

The following example demonstrates the basic usage of the Logs-parser tool. It reads infile.txt, processes it in a way, that TestRawParser does and writes results in outfile.txt in format, defined in ResultToStringConverter. 

```
#!typescript

import * as fs from 'fs';
import {SyncFileReader, TestRawParser, ResultToStringConverter, SyncFileWriter, TestLogParser,
        getTimestamp, getClientMessageType, getSessionID} from './index';

let rawParser = new TestRawParser(getClientMessageType, getTimestamp, getSessionID);
let syncFileReader = new SyncFileReader(rawParser, fs);
let syncFileWriter = new SyncFileWriter(fs);
let resultToStringConverter = new ResultToStringConverter(JSON);
let logParser = new TestLogParser(syncFileReader, syncFileWriter, resultToStringConverter);

logParser.process("infile.txt", "outfile.txt", function(err) {
    if (err) {
        console.log(err);
    }
});
```

## Prerequisites

The one needs to install NodeJS and npm to run this software. NodeJS needs to be of version 0.8.15 which can be installed via nvm (Node Version Manger). Nvm can be downloaded from https://github.com/creationix/nvm or https://github.com/coreybutler/nvm-windows (for Linux/MacOS or Windows accordingly).

TypeScript compiler is also needed and can be installed via npm with the following command:

```
#!bash

npm install -g typescript
```

## Dependencies
The following libraries are used in the tool and can be installed with *npm install* command:

* async
* continuate

The developer dependencies:

* jasmine
* sinon